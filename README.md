# Rounds

Configurable countdown timer app for Ubuntu Touch, cycling between action rounds and breaks.

## License

Copyright (C) 2022  Lothar Ketterer

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## How to build

### IMPORTANT

This is the version for focal (20.04). To build for xenial (16.04), change to the xenial branch.

### Instructions

The click package can be built by using clickable <https://clickable-ut.dev/en/latest/>:

`clickable build`

It's a qml only app, so the resulting package is suitable for all arches.

More detailed instructions regarding clickable can be found, e.g., in this tutorial: <https://ubports.gitlab.io/marketing/education/ub-clickable-1/trainingpart1module1.html#_on_your_ubuntu_touch_phone>

## How to install

The easiest way to install the current release is via OpenStore.

To install the git version, use `clickable` to build it yourself as described above.
